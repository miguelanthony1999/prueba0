﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibrosDescuento.Entities
{
  public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? SerieId { get; set; }
        public decimal Price { get; set; }

        public Serie Serie { get; set; }

    }
}