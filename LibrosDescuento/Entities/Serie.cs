﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibrosDescuento.Entities
{
    public class Serie
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}